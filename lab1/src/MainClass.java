import javafx.util.Pair;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class MainClass {
    private static final int PORT = 17120;
    private static final int TIMEOUT = 3000;
    private static final int TIMELIVE = 10000;

    public static void main(String[] args) {
        UUID uuid = UUID.randomUUID();
        //System.out.println(uuid.toString());
        MulticastSocket socket = null;
        try {
            try {
                socket = new MulticastSocket(PORT);
                socket.joinGroup(InetAddress.getByName(args[0]));
            } catch (SocketException e) {
                System.err.println(e.toString());
                return;
            }
            byte[] data = uuid.toString().getBytes();
            DatagramPacket sendPacket = new DatagramPacket(data, data.length, InetAddress.getByName(args[0]), PORT);
            DatagramPacket receivePacket = new DatagramPacket(new byte[data.length], data.length);
            HashMap<Pair<String,InetAddress>, Long> container = new HashMap<>();
            socket.setSoTimeout(3000);//для разблокировки receive
            long lastTime = System.currentTimeMillis();

            while (true) {
                if (System.currentTimeMillis() - lastTime > TIMEOUT) {
                    lastTime = System.currentTimeMillis();
                    socket.send(sendPacket);
                    if (checkContainer(container)) {
                        printContainer(container, "environment reduced");
                    }
                }
                try {
                    socket.receive(receivePacket);
                    Pair<String, InetAddress> pair = new Pair<>(Arrays.toString(receivePacket.getData()), receivePacket.getAddress());
                    if (!container.containsKey(pair)) { //если не было в мапе
                        container.put(pair, System.currentTimeMillis());
                        printContainer(container, "environment increased");
                    } else {
                        container.put(pair, System.currentTimeMillis());
                    }
                } catch (SocketTimeoutException ignored) {
                }
            }
        } catch (IOException e) {
            System.err.println(e.toString());
            if (socket != null) {
                socket.close();
            }
        }
    }

    private static boolean checkContainer(HashMap<Pair<String, InetAddress>, Long> container) {
        boolean flag = false;
        for (Iterator<HashMap.Entry<Pair<String, InetAddress>, Long>> it = container.entrySet().iterator(); it.hasNext(); ) {
            HashMap.Entry<Pair<String, InetAddress>, Long> var = it.next();
            if (System.currentTimeMillis() - var.getValue() > TIMELIVE) {
                it.remove();
                flag = true;
            }
        }
        return flag;
    }

    private static void printContainer(HashMap<Pair<String, InetAddress>, Long> container, String message) {
        System.out.println();
        System.out.println(message);
        System.out.println("В сети " + container.size() + " копий программы:");
        for (Map.Entry<Pair<String, InetAddress>, Long> var : container.entrySet()) {
            System.out.println(UUID.nameUUIDFromBytes(var.getKey().getKey().getBytes()));
            System.out.println(var.getKey().getValue().getHostAddress());
        }
        System.out.println();
    }
}